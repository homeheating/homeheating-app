import 'package:flutter/material.dart';
import 'package:home_heating/home_heating.dart';
import 'package:home_heating/main.dart';
import 'package:http/http.dart' as http;

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<String>>(
      future: search(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Ошибка: ${snapshot.error}'));
        } else {
          return SafeArea(
            child: Center(
              child: Column(
                children: List.generate(
                  snapshot.data!.length,
                  (index) => ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HeatingPage(dataBloc: dataBloc, relayBloc: relayBloc, sendBloc: sendBloc, ip: snapshot.data![index]),
                        ),
                      );
                    },
                    child: Text(snapshot.data![index]),
                  ),
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Future<List<String>> search() async {
    final List<String> foundDevices = [];
    const String subnet = '192.168.1';
    for (var i = 0; i < 256; i++) {
      final String url = 'http://$subnet.$i:80/';
     // print('Scanning IP: $url');
      try {
        final response = await http.get(Uri.parse(url)).timeout(const Duration(milliseconds: 500));
      //  print('Body: ${response.body}');
        if (response.body.contains('hello from Sonoff BasicR2!')) {
        //  print('Device found at address: $url');
          foundDevices.add(i.toString());
        }
      } catch (e) {
      //  print('Error: $e');
      }
    }
    return foundDevices;
  }
}