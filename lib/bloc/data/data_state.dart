part of 'data_bloc.dart';

@immutable
sealed class DataState {}

final class DataInitial extends DataState {}

class GetTempAndHumState extends DataState {
  final int temperature;
  final int humidity;

  GetTempAndHumState(this.temperature, this.humidity);
}

class GetMinMaxState extends DataState{
  final int minTemp;
  final int maxTemp;

  GetMinMaxState(this.minTemp, this.maxTemp);
}
class LoadingTempAndHumState extends DataState {}

class ErrorState extends DataState {
  final String errorMessage;

  ErrorState(this.errorMessage);
}