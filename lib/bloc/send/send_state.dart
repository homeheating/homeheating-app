part of 'send_bloc.dart';

@immutable
sealed class SendState {}

final class SendInitial extends SendState {}
class SendMinMaxState extends SendState {
  final int maxTemp;
  final int minTemp;

  SendMinMaxState(this.maxTemp, this.minTemp);

}
