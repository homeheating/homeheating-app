part of 'send_bloc.dart';

@immutable
sealed class SendEvent {}
class SendMinMaxEvent extends SendEvent{
  final int maxTemp;
  final int minTemp;

  SendMinMaxEvent(this.maxTemp, this.minTemp);

}
class UpdateMinMaxEvent extends SendEvent {
  final int maxTemp;
  final int minTemp;

  UpdateMinMaxEvent(this.maxTemp, this.minTemp);
}