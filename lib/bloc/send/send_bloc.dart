import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:home_heating/data.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:stream_transform/stream_transform.dart';

part 'send_event.dart';
part 'send_state.dart';

EventTransformer<E> debounce<E>(Duration duration){
  return (events,mapper){
    return droppable<E>().call(events.debounce(duration), mapper);
  } ;
  
}

class SendBloc extends Bloc<SendEvent, SendState> {
  SendBloc() : super(SendInitial()) {
    on<SendMinMaxEvent>(_sendMinMax, transformer: debounce(const Duration(seconds: 4)));
    on<UpdateMinMaxEvent>(_updateMinMax);
  }

  _updateMinMax(UpdateMinMaxEvent event, Emitter<SendState> emitter) async {
    // Просто отправляем новый SendMinMaxState с полученными значениями
     emitter(SendMinMaxState(event.maxTemp, event.minTemp));
  
  }

  _sendMinMax(SendMinMaxEvent event, Emitter<SendState> emitter) async {
const url = "$webApiUrl/settemperature";
emitter(SendMinMaxState(event.maxTemp, event.minTemp));
final Map<String, dynamic> requestData = {
    "maxtemperature": event.maxTemp.toDouble(),
    "mintemperature": event.minTemp.toDouble(),
  };
    try {
      final response = await http.Client()
          .post(Uri.parse(url),
          body: json.encode(requestData), // Кодируем Map в JSON
          headers: {
            'Content-Type': 'application/json', // Устанавливаем заголовок Content-Type
          },
        )
          .timeout(const Duration(seconds: 5));

       if (response.statusCode == 200) {
      // Запрос успешно выполнен, обработайте успешный ответ здесь
    } else {
      // Запрос завершился с ошибкой, обработайте ошибку здесь
    }

      

     // emit(GetRelayState(isRelayOn: isRelayOn));
    } catch (e) {
      // print('Ошибка при запросе данных: $e');
     // emit(ErrorState('Ошибка при запросе данных: $e'));
    }
  }
}
